import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import appConfig from './config/app-module.config';
import throttlerConfig from './config/throttler.config';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './model/user/user.module';
import config from './config/env.config';
import options from './config/mongoos.config';
@Module({
  imports: [
    MongooseModule.forRoot(`${config().database.host}/vogroup`, options),
    ConfigModule.forRoot(appConfig),
    ThrottlerModule.forRoot(throttlerConfig),
    UserModule,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
    AppService,
  ],
})
export class AppModule {}
