import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { PaginationDto, QueryDto, UserFindDto } from './dto/user.find.dto';
import { UserCreateDto } from './dto/user.create.dto';
import { UserUpdateDto } from './dto/user.update.dto';
import { UserService } from './user.service';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  async findAll() {
    return await this.userService.findAll({});
  }

  @Get(':id')
  async find(@Param() finduserDto: UserFindDto) {
    return await this.userService.findOne(finduserDto.id);
  }

  @Post()
  async create(@Body() userCreateDto: UserCreateDto) {
    return await this.userService.create(userCreateDto);
  }

  @Put(':id')
  async update(@Param() finduserDto: UserFindDto, @Body() userUpdateDto: UserUpdateDto) {
    return await this.userService.update(finduserDto.id, userUpdateDto);
  }

  @Delete(':id')
  async delete(@Param() finduserDto: UserFindDto) {
    return await this.userService.delete(finduserDto.id);
  }
}
