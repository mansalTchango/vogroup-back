import { IsDate, IsEmail, IsOptional, IsString } from 'class-validator';
import { IsValidObjectId } from '../../../decorator/isValidObjectId.decorator';

export class UserBaseDto {
  @IsString()
  @IsValidObjectId()
  @IsOptional()
  readonly id?: string;

  @IsString()
  readonly firstName: string;

  @IsString()
  readonly lastName: string;

  @IsEmail()
  readonly email: string;

  @IsString()
  readonly password: string;

  @IsDate()
  @IsOptional()
  readonly createdAt?: Date;

  @IsDate()
  @IsOptional()
  readonly updatedAt?: Date;
}
