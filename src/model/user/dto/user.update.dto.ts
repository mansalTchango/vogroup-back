import { IsDate, IsOptional, IsString } from 'class-validator';

export class UserUpdateDto {
  @IsString()
  @IsOptional()
  readonly firstName?: string;

  @IsString()
  @IsOptional()
  readonly lastName?: string;

  @IsString()
  @IsOptional()
  readonly password?: string;

  @IsDate()
  @IsOptional()
  readonly updatedAt?: Date;
}
