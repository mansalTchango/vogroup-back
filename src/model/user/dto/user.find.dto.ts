import { Transform, Type } from 'class-transformer';
import { IsNumber, IsObject, IsOptional, IsString, Min } from 'class-validator';
import { stringToObject } from '../../../utils/stringToObject.util';
import { IsValidObjectId } from '../../../decorator/isValidObjectId.decorator';

export class UserFindDto {
  @IsString()
  @IsValidObjectId()
  readonly id: string;
}

export class PaginationDto {
  @IsNumber()
  @Min(1)
  @Type(() => Number)
  readonly page: number;

  @IsNumber()
  @Min(3)
  @Type(() => Number)
  readonly limit: number;
}

export class QueryDto {
  @IsObject()
  @IsOptional()
  @Transform(({ value }) => JSON.parse(value))
  readonly where?: object;

  @IsObject()
  @IsOptional()
  @Transform(({ value }) => stringToObject(value))
  readonly select?: object;
}
