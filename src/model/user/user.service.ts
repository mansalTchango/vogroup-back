import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserBaseDto } from './dto/user.base.dto';
import { UserCreateDto } from './dto/user.create.dto';
import { UserUpdateDto } from './dto/user.update.dto';
import { IUser } from './interfaces/User.interface';
interface Query {
  where?: object;
  skip?: number;
  limit?: number;
  select?: object;
}
@Injectable()
export class UserService {
  constructor(@InjectModel('users') private readonly model: Model<IUser>) {}

  async findAll({ where, skip, limit, select }: Query): Promise<IUser[]> {
    return await this.model.find(where).sort({ _id: 1 }).skip(skip).limit(limit).select(select).exec();
  }

  async findOne(id: string, field?: object): Promise<IUser> {
    return await this.model.findById(id).select(field).exec();
  }

  async create(userCreateDto: UserCreateDto): Promise<UserBaseDto> {
    return await new this.model({
      ...userCreateDto,
      createdAt: new Date(),
    }).save();
  }

  async update(id: string, userUpdateDto: UserUpdateDto): Promise<IUser> {
    return await this.model.findByIdAndUpdate(id, { ...userUpdateDto, updatedAt: new Date() }, { new: true }).exec();
  }

  async delete(id: string): Promise<IUser> {
    return await this.model.findByIdAndDelete(id).exec();
  }
}
