import * as mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
  id: String,
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  createdAt: Date,
  updatedAt: Date,
});

export default UserSchema;
