import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsInArray', async: true })
@Injectable()
export class IsInArrayConstraint implements ValidatorConstraintInterface {
  async validate(value: string, args: ValidationArguments) {
    if (args.constraints.includes(value)) return true;
    return false;
  }

  defaultMessage(args: ValidationArguments) {
    return `${args.property} must be in array : [${args.constraints}]`;
  }
}

export function IsInArray(array: any[], validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: array,
      validator: IsInArrayConstraint,
    });
  };
}
