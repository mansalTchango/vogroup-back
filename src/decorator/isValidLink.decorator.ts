import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import HttpStatusCode from '../types/httpStatusCode.type';

@ValidatorConstraint({ name: 'IsValidLink', async: true })
@Injectable()
export class IsValidLinkConstraint implements ValidatorConstraintInterface {
  constructor(private status: number) {}

  async validate(value: string) {
    try {
      this.status = await getURLStatus(value);
      if (this.status !== HttpStatusCode.OK) return false;
    } catch (e) {
      this.status = 404;
      return false;
    }
    return true;
  }

  defaultMessage(args: ValidationArguments) {
    return `${args.property} must be a valid link but the status returned is ${this.status}`;
  }
}

export async function getURLStatus(url: string) {
  const res = await fetch(url);
  return res.status;
}

export function IsValidLink(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsValidLinkConstraint,
    });
  };
}
