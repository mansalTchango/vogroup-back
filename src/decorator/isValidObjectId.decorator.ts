import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Types } from 'mongoose';

@ValidatorConstraint({ name: 'IsValidObjectId', async: true })
@Injectable()
export class IsValidObjectIdConstraint implements ValidatorConstraintInterface {
  async validate(value: string) {
    try {
      if (
        !Types.ObjectId.isValid(value) &&
        new Types.ObjectId(value).toString() !== value
      ) {
        return false;
      }
    } catch (e) {
      return false;
    }
    return true;
  }

  defaultMessage(args: ValidationArguments) {
    return `${args.property} must be a objectId valid`;
  }
}

export function IsValidObjectId(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsValidObjectIdConstraint,
    });
  };
}
