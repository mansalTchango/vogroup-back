const config = () => ({
  app: {
    port: parseInt(process.env.PORT, 10) || 3005,
  },
  database: {
    host: process.env.HOST || 'mongodb://localhost',
  },
});

export default config;
