const throttlerConfig = {
  ttl: 60,
  limit: 100,
};

export default throttlerConfig;
