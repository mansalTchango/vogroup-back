import config from './env.config';

const appConfig = {
  isGlobal: true,
  load: [config],
};

export default appConfig;
