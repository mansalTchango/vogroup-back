import { DocumentBuilder } from '@nestjs/swagger';

const swaggerConfig = new DocumentBuilder()
  .setTitle('TEST VOGROUP API')
  .setDescription('Test backend for Vogroup with NestJS')
  .setVersion('1.0')
  .build();

export default swaggerConfig;
