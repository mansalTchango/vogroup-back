export function stringToObject(str: string) {
  const myArray = str.split(',');
  const result = {};

  for (const key of myArray) {
    result[`${key}`] = 1;
  }

  return result;
}
